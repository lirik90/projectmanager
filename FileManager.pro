#-------------------------------------------------
#
# Project created by QtCreator 2015-07-03T08:03:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11 qscintilla2

TARGET = FileManager
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    treemodel.cpp \
    loglabel.cpp \
    parsethread.cpp \
    imagewidget.cpp \
    htmleditor.cpp \
    coloredcalendar.cpp \
    dirmodel.cpp \
    fswatcherthread.cpp

HEADERS  += mainwindow.h \
    treemodel.h \
    loglabel.h \
    parsethread.h \
    imagewidget.h \
    htmleditor.h \
    coloredcalendar.h \
    dirmodel.h \
    fswatcherthread.h

FORMS    += mainwindow.ui

win32:{
    RC_ICONS = images/main.ico
    QMAKE_TARGET_COMPANY = Zimnikov
    QMAKE_TARGET_DESCRIPTION = File Manager
    QMAKE_TARGET_COPYRIGHT = Zimnikov
    QMAKE_TARGET_PRODUCT = FileManager
    RC_LANG = 0x0419
    RC_CODEPAGE = 1200
}

unix:{
    LIBS += -lqscintilla2
}

RESOURCES += \
    filemanager.qrc
