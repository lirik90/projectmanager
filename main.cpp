#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("Zimnikov");
    QCoreApplication::setOrganizationDomain("zimnikov.ru");
    QCoreApplication::setApplicationName("FileManager");
    QCoreApplication::setApplicationVersion("1.0");

    MainWindow w;
    w.show();

    return a.exec();
}
