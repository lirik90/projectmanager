#include <QSettings>
#include <QStandardItemModel>
#include <QDirIterator>
#include <QTextStream>
#include <QTextCodec>
#include <QDebug>
#include <QElapsedTimer>
#include <QTextDocument>
#include <QFileSystemWatcher>

#include <functional>

#include "dirmodel.h"
#include "parsethread.h"

ParseThread::ParseThread(QString path, DirModel* treeModel, QFileSystemWatcher *watcher,
                         bool refresh, QObject *parent) :
    QThread(parent),
    parsePath(path),
    model(treeModel),
    fsWatcher(watcher),
    canceled(false),
    isRefresh(refresh)
{
    connect(this, SIGNAL(finished()), SLOT(deleteLater()));
}

void ParseThread::run()
{
    emit message("Парсим папку...");
    if (!canceled)
    {
        fsWatcher->addPath( parsePath );

        QVector< DirNode* > removeList;
        model->goAllNode([&](DirNode* nodeItem) -> bool
        {
            if ( !QFile::exists( nodeItem->toolTip ) )
                removeList << nodeItem;
            return false;
        });

        for (DirNode* node : removeList)
            model->removeNode( node );

        SubDirNode* subNode = nullptr;
        DirNode* parent = model->nodeFromPath( parsePath, &subNode );
        if ( !parent )
            parent = model->getRootNode();
        parseDir( parsePath, parent, subNode );
    }

    if (!canceled) emit message("Обработка завершена.");
    else emit message("Отменено.");
}

void ParseThread::parseDir(QString path, DirNode *parentNode, SubDirNode* parentItem)
{
    QDirIterator dirIt( path, QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot );

    while (dirIt.hasNext())
    {
        if (canceled) return;

        QFileInfo fi = dirIt.next();
        cFileName = fi.fileName();
        fsWatcher->addPath( fi.filePath() );

        if ( fi.isFile() && parentNode )
        {
            if ( cFileName == "%description.txt" )
                parentNode->descriptionFile = fi.filePath();
            else if ( parentItem )
            {
                SubDirNode* item = nullptr;
                parentNode->model.goAllNode(parentItem, [&](SubDirNode* node) -> bool {
                    if ( node->toolTip == fi.filePath() ) {
                        item = node;
                        return true;
                    }
                    return false;
                });

                if ( !item ) {
                    item = new SubDirNode;
                    item->name = cFileName;
                    item->fileName = fi.filePath();
                    item->toolTip = fi.filePath();

                    if ( fi.suffix() == "html" )
                        item->type = SimpleNode< SubDirNode >::HTML;
                    else if ( fi.suffix() == "jpg" || fi.suffix() == "jpeg" ||
                              fi.suffix() == "png" || fi.suffix() == "bmp" )
                        item->type = SimpleNode< SubDirNode >::Image;
                    else if ( fi.suffix() == "txt" || fi.suffix().isEmpty() )
                        item->type = SimpleNode< SubDirNode >::Text;
                    else
                        item->type = SimpleNode< SubDirNode >::Other;

                    parentNode->model.insertNode( item, parentItem );
                    if ( isRefresh )
                        parentNode->model.setNodeState(item, SubDirNode::New);
                }
            }
        }
        else if ( fi.isDir() )
        {
            QChar c = cFileName.at(0);
            cFileName.remove(0, 1);

            if ( c == '#' )
            {
                QStringList sl = cFileName.split('#');

                DirNode* node = nullptr;
                model->goAllNode(parentNode, [&](DirNode* nodeItem) -> bool {
                    if ( nodeItem->toolTip == fi.filePath() ) {
                        node = nodeItem;
                        return true;
                    }
                    return false;
                });

                if ( !node )
                {
                    node = model->addNode( sl[0], parentNode );
                    node->toolTip = fi.filePath();
                    if ( isRefresh )
                        model->setNodeState(node, DirNode::New);

                    if ( sl.size() > 1 )
                    {
                        QDate date;
                        for (const QString& str: sl)
                        {
                            if ( str.contains("active", Qt::CaseInsensitive) )
                                node->active = true;
                            else {
                                date = QDate::fromString( str, "d.M.yyyy" );
                                if ( date.isValid() ) {
                                    if ( node->from.isValid() )
                                        node->to = date;
                                    else
                                        node->from = date;
                                }
                            }
                        }
                    }
                    else if ( node->parent->from.isValid() )
                    {
                        node->from = node->parent->from;
                        node->to = node->parent->to;
                        node->active = node->parent->active;
                    }
                }

                parseDir( fi.filePath(), node );
            }
            else if ( c == '%' && parentNode )
            {
                SubDirNode* item = nullptr;
                parentNode->model.goAllNode([&](SubDirNode* node) -> bool {
                    if ( node->toolTip == fi.filePath() ) {
                        item = node;
                        return true;
                    }
                    return false;
                });

                if ( !item ) {
                    item = parentNode->model.addNode( cFileName );
                    item->toolTip = fi.filePath();
                }

                parseDir( fi.filePath(), parentNode, item );
            }
        }
    }
}
