#ifndef HTMLEDITOR_H
#define HTMLEDITOR_H

#include <Qsci/qsciscintilla.h>

class HTMLEditor : public QsciScintilla
{
    Q_OBJECT
public:
    explicit HTMLEditor(QWidget *parent = 0);
private:
    void keyPressEvent(QKeyEvent *e);
signals:

public slots:
};

#endif // HTMLEDITOR_H
