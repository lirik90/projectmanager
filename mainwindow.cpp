#include <QDebug>
#include <QSettings>
#include <QDir>
#include <QFileDialog>
#include <QProgressBar>
#include <QPushButton>
#include <QMessageBox>

#include "fswatcherthread.h"
#include "parsethread.h"
#include "dirmodel.h"
#include "loglabel.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

const char changedProperty[] = "changed";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    parseThread(nullptr)
{
    ui->setupUi(this);
    ui->subTree->addAction( ui->actionRename );
    ui->subTree->addAction( ui->actionDel );

    model = new DirModel(this);
    ui->dirTree->setModel( model );
    connect(ui->dirTree->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            SLOT(setCurrentModelIndex(QModelIndex,QModelIndex)));

    fsWatcherTh = new FSWatcherThread(model, this);
    connect(fsWatcherTh, SIGNAL(needUpdate(QString)), SLOT(parse(QString)));

    connect(ui->htmlEdit, SIGNAL(textChanged()), SLOT(enableSave()));
    connect(ui->textEdit, SIGNAL(textChanged()), SLOT(enableSave()));
    connect(ui->descEdit, SIGNAL(textChanged()), SLOT(enableSave()));

    fileNameLbl = new LogLabel;
    pbar = new QProgressBar;
    cancelBtn = new QPushButton("Отменить");
    hideProgress();

    ui->sBar->addWidget(fileNameLbl, 1);
    ui->sBar->addWidget(pbar, 2);
    ui->sBar->addWidget(cancelBtn);    
    connect(ui->sBar, SIGNAL(messageChanged(QString)),
            fileNameLbl, SLOT(setText(QString)));

    QSettings settings;
    settings.beginGroup("MainWindow");
    restoreGeometry(settings.value("geometry").toByteArray());
    bool maximize = settings.value("maximized", false).toBool();
    if (maximize)
        setWindowState(Qt::WindowMaximized);

    ui->splitter->restoreState(settings.value("splitterState").toByteArray());
    ui->splitter2->restoreState(settings.value("splitter2State").toByteArray());
    settings.endGroup();
}

MainWindow::~MainWindow()
{
    fsWatcherTh->quit();
    fsWatcherTh->wait();
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QSettings settings;
    settings.beginGroup("MainWindow");
    settings.setValue("geometry", saveGeometry() );
    settings.setValue("maximized", isMaximized() );
    settings.setValue("splitterState", ui->splitter->saveState() );
    settings.setValue("splitter2State", ui->splitter2->saveState() );
    settings.endGroup();
    QMainWindow::closeEvent(event);
}

void MainWindow::show()
{
    QMainWindow::show();

    QSettings s;
    QString path = s.value("lastPath").toString();
    if (!path.isEmpty())
        parseDirectory( path );
}

void MainWindow::hideProgress(bool visable)
{
    pbar->setVisible( visable );
    cancelBtn->setVisible( visable );

    emit model->dataChanged(QModelIndex(), QModelIndex());
}

void MainWindow::on_actionChooseFolder_triggered()
{
    QSettings s;
    QString path = s.value("lastPath", QDir::homePath()).toString();

    QString dir = QFileDialog::getExistingDirectory(this, "Выберите дерикторию", path);
    if (!dir.isEmpty())
        parseDirectory( dir );
}

void MainWindow::setCurrentModelIndex(QModelIndex index, QModelIndex prev)
{
    if ( DirNode* node = model->nodeFromIndex( index ) )
    {
        if ( checkSave() ) {
            DirNode* nodePrev = model->nodeFromIndex( prev );
            if ( nodePrev )
                saveChanges( nodePrev, nodePrev->model.nodeFromIndex( ui->subTree->currentIndex() ) );
        }

        if ( !node->descriptionFile.isEmpty() )
            ui->descEdit->setPlainText( getFileText( node->descriptionFile ) );
        ui->actionSave->setEnabled( false );

        if ( node->state != DirNode::NoState )
            model->unstateNode( node );

        if ( ui->subTree->selectionModel() )
            disconnect(ui->subTree->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)), 0, 0);
        ui->subTree->setModel( &node->model );
        ui->subTree->expandAll();
        connect(ui->subTree->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),
                SLOT(setCurrentSubModelIndex(QModelIndex,QModelIndex)));

        ui->calendarWidget->setColoredRange( node );
        ui->calendarWidget->setFocus();
    }
}

void MainWindow::setCurrentSubModelIndex(QModelIndex index, QModelIndex prev)
{
    SubDirModel* subModel = static_cast<SubDirModel*>( ui->subTree->model() );
    SubDirNode *node = subModel->nodeFromIndex( index );
    if ( !node )
        return;

    if ( checkSave() )
        saveChanges( model->nodeFromIndex( ui->dirTree->currentIndex() ),
                     subModel->nodeFromIndex( prev ) );
    ui->stackedWidget->setCurrentIndex( node->type );

    switch ( node->type ) {
    case SubDirNode::Folder:
    case SubDirNode::Other:
        break;
    case SubDirNode::HTML:
        ui->htmlEdit->setText( getFileText( node->fileName ) );
        break;
    case SubDirNode::Image:
        ui->imageView->setPixmap(QPixmap( node->fileName ));
        break;
    case SubDirNode::Text:
        ui->textEdit->setPlainText( getFileText( node->fileName ) );
        break;
    }

    ui->actionSave->setEnabled( false );

    if ( node->state != SubDirNode::NoState &&
         node->type != SubDirNode::Folder)
    {
        subModel->setNodeState( node, SubDirNode::NoState );
        model->setNodeState( subModel, DirNode::NoState );
    }
}

void MainWindow::parseDirectory(QString path)
{
    if ( !QDir( path ).exists() )
        return;

    fsWatcherTh->restart();
    model->clear();

    QSettings s;
    s.setValue("lastPath", path);

    parse(path, false);
}

bool MainWindow::checkSave()
{
    if ( !ui->actionSave->isEnabled() )
        return false;

    int ret = QMessageBox::warning(this, "Несохранённые изменения",
                                   "Несохранённые изменения будут утеряны.\nСохранить?",
                                   QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
    return ret == QMessageBox::Yes;
}

void MainWindow::saveChanges(DirNode* node, SubDirNode *subNode)
{
    auto WriteFile = [&](QString fileName, QString data)
    {
        QFile file( fileName );
        if ( file.open( QIODevice::WriteOnly ) )
        {
            file.write( data.toLocal8Bit() );
        }
        else
            QMessageBox::critical(this, "Ошибка открытия файла", file.errorString() );
    };

    if ( node )
    {
        if ( ui->descEdit->property( changedProperty ).toInt() == 1 ) {
            WriteFile( node->descriptionFile, ui->descEdit->toPlainText() );
            ui->descEdit->setProperty( changedProperty, 0 );
        }
    }

    if ( subNode )
    {
        if ( ui->htmlEdit->property( changedProperty ).toInt() == 1 ) {
            WriteFile( subNode->toolTip, ui->htmlEdit->text() );
            ui->htmlEdit->setProperty( changedProperty, 0 );
        }

        if ( ui->textEdit->property( changedProperty ).toInt() == 1 ) {
            WriteFile( subNode->toolTip, ui->textEdit->toPlainText() );
            ui->textEdit->setProperty( changedProperty, 0 );
        }
    }
}

QString MainWindow::getFileText(const QString &fileName)
{
    QFile file( fileName );
    if ( file.open(QIODevice::ReadOnly | QIODevice::Text) )
        return file.readAll();
    else
        fileNameLbl->setText( file.errorString() );
    return QString();
}

void MainWindow::parse(const QString &path, bool update)
{
    if ( parseThread )
        return;

    hideProgress( true );
    pbar->setRange(0, 0);

    parseThread = new ParseThread( path, model, fsWatcherTh->fsWatcherPtr, update );
    connect(parseThread, SIGNAL(finished()), SLOT(parseFinished()));
    connect(parseThread, SIGNAL(message(QString)), fileNameLbl, SLOT(setText(QString)));
    connect(cancelBtn, SIGNAL(clicked()), parseThread, SLOT(cancel()));
    parseThread->start();
}

void MainWindow::parseFinished()
{
    fileNameLbl->setText("");
    hideProgress();
    ui->dirTree->expandAll();
    parseThread = nullptr;
}

void MainWindow::on_actionSave_triggered()
{
    ui->actionSave->setEnabled( false );

    DirNode* node = model->nodeFromIndex( ui->dirTree->currentIndex() );
    SubDirNode *subNode = node->model.nodeFromIndex( ui->subTree->currentIndex() );
    saveChanges( node, subNode );
}

void MainWindow::enableSave()
{
    sender()->setProperty( changedProperty, 1 );
    ui->actionSave->setEnabled( true );
}

void MainWindow::on_actionDel_triggered()
{
    DirNode* node = model->nodeFromIndex( ui->dirTree->currentIndex() );
    SubDirNode *subNode = node->model.nodeFromIndex( ui->subTree->currentIndex() );
    if ( subNode )
        QFile::remove( subNode->toolTip );
}
