#ifndef LOGLABEL_H
#define LOGLABEL_H

#include <QLabel>

class LogLabel : public QLabel
{
    Q_OBJECT
public:
    explicit LogLabel(QWidget *parent = 0);
private:
    QStringList sl;
signals:

public slots:
    void setText(const QString &text);

    // QWidget interface
protected:
    void mouseDoubleClickEvent(QMouseEvent *);
};

#endif // LOGLABEL_H
