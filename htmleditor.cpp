#include <Qsci/qscilexerhtml.h>
#include <QKeyEvent>

#include "htmleditor.h"

HTMLEditor::HTMLEditor(QWidget *parent) : QsciScintilla(parent)
{
    setUtf8( true );

    QsciLexerHTML* lexer = new QsciLexerHTML( this );
#ifdef Q_OS_LINUX
    lexer->setFont( QFont("Monospace") ); // шрифт лексера
#else
    lexer->setFont( QFont("Courier New") ); // шрифт лексера
#endif
    setLexer( lexer );

    //! Текущая строка кода и ее подсветка
    setCaretLineVisible(true);
    setCaretLineBackgroundColor(QColor("gainsboro"));

    //! Выравнивание
    setAutoIndent(true);
    setIndentationGuides(false);
    setIndentationsUseTabs(true);
    setIndentationWidth(4);

    //! margin это полоска слева, на которой обычно распологаются breakpoints
    setMarginsBackgroundColor(QColor("gainsboro"));
    setMarginLineNumbers(1, true);
    setMarginWidth(1, 45);

    //! Autocompleting
    setAutoCompletionSource(QsciScintilla::AcsAll); // автокомплитим из всех источников
    setAutoCompletionCaseSensitivity(true); // конечно чувствительны к регистру
    setAutoCompletionReplaceWord(true); // удаляем слово которое дополняем (пишем заново)
    setAutoCompletionUseSingle(AcusExplicit); // исли в списке один элемент, то сразу дополняем
    setAutoCompletionThreshold(1); // автокомплит после 1го введеного символа

    //! Подсветка соответствий скобок
    setBraceMatching(QsciScintilla::SloppyBraceMatch);
    setMatchedBraceBackgroundColor(Qt::yellow);
    setUnmatchedBraceForegroundColor(Qt::blue);

    setFolding( BoxedTreeFoldStyle );
}

void HTMLEditor::keyPressEvent(QKeyEvent *e)
{
    if((e->modifiers() == Qt::CTRL) && (e->key() == Qt::Key_Space)) { // Ctrl+<Space>
        autoCompleteFromAll();
        return; // не дает дописать NULL
    }

    QsciScintilla::keyPressEvent(e);
}
