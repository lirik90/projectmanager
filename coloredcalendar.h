#ifndef COLOREDCALENDAR_H
#define COLOREDCALENDAR_H

#include <QCalendarWidget>

class DirNode;

class ColoredCalendar : public QCalendarWidget
{
    Q_OBJECT
public:
    explicit ColoredCalendar(QWidget *parent = 0);
    void setColoredRange(DirNode* dirNode);
protected:
    void paintCell(QPainter *painter, const QRect &rect, const QDate &date) const;
private:
    DirNode* node;
};

#endif // COLOREDCALENDAR_H
