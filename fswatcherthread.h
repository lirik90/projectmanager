#ifndef FSWATCHERTHREAD_H
#define FSWATCHERTHREAD_H

#include <QThread>
#include <QFileSystemWatcher>

class QFileSystemWatcher;
class DirModel;

class FSWatcherThread : public QThread
{
    Q_OBJECT
public:
    explicit FSWatcherThread(DirModel* dirModel, QObject *parent = 0);
    void restart();
    QFileSystemWatcher* fsWatcherPtr;
private:
    void run();
    DirModel* model;
signals:
    void needUpdate(QString path);
};

class FSWatcher : public QFileSystemWatcher
{
    Q_OBJECT
public:
    FSWatcher(DirModel* dirModel, QObject *parent = 0);
private:
    DirModel* model;
private slots:
    void fileChangedSlot(const QString &path);
    void directoryChangedSlot(const QString &path);
signals:
    void needUpdate(QString path);
};
#endif // FSWATCHERTHREAD_H
