#include <QDialog>
#include <QPlainTextEdit>
#include <QBoxLayout>

#include "loglabel.h"

LogLabel::LogLabel(QWidget *parent) :
    QLabel(parent)
{

}

void LogLabel::setText(const QString &text)
{
    QLabel::setText( text );
    sl << text;

    if ( sl.count() > 1000 )
        sl.removeFirst();
}


void LogLabel::mouseDoubleClickEvent(QMouseEvent *)
{
    QPlainTextEdit* te = new QPlainTextEdit;
    te->setWordWrapMode( QTextOption::WordWrap );
    te->setPlainText( sl.join('\n') );
    te->show();

    QHBoxLayout* hbox = new QHBoxLayout;
    hbox->addWidget( te );
    QDialog d;
    d.setLayout( hbox );
    d.resize(500, 600);
    d.exec();
}
