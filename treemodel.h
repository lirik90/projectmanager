﻿#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>
#include <QIcon>
#include <QBrush>
#include <functional>

template< class T >
class TemplateNode
{
public:
    TemplateNode() : parent(nullptr) {}
    ~TemplateNode() {
        qDeleteAll(children);
    }

    void addChild(T *p) {
        p->parent = static_cast< T* >( this );
        children += p;
    }

    T *parent;
    QList<T *> children;
};

template< class T >
class SimpleNode : public TemplateNode< T >
{
public:
    SimpleNode() : TemplateNode< T >(), state(NoState) {}
    enum Types { HTML = 0, Image, Text, Folder, Other };
    enum State { NoState = 0, New, Edited };

    QString name;
    QString toolTip;

    Types type;
    State state;
};

template< class Node >
class TreeModel : public QAbstractItemModel
{
public:
    explicit TreeModel(QObject *parent = 0) :
        QAbstractItemModel(parent)
    {
        rootNode = new Node;
    }
    ~TreeModel()
    {
        delete rootNode;
    }

    Node *addNode(QString name, Node* parent = nullptr)
    {
        if ( !parent )
            parent = rootNode;

        Node* node = new Node;
        node->name = name;
        node->type = SimpleNode< Node >::Folder;

        parent->addChild( node );
        return node;
    }

    void insertNode(Node *node, Node *parent)
    {
        if ( !parent )
            parent = rootNode;

        int row = parent->children.count();
        QModelIndex parentIdx = indexFromNode( parent );
        beginInsertRows( parentIdx, row, row );
        parent->addChild( node );
        endInsertRows();
    }

    void clear()
    {
        beginResetModel();
        delete rootNode;
        rootNode = new Node;
        endResetModel();
    }
    Node *getRootNode()
    {
        return rootNode;
    }

    static void goAllNode(Node* parent, std::function< bool(Node *)> func)
    {
        for (Node* node: parent->children)
        {
            if ( func( node ) )
                return;
            goAllNode( node, func );
        }
    }
    void goAllNode(std::function< bool(Node *) > func) {
        goAllNode( getRootNode(), func );
    }

    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const
    {
        if (!rootNode || row < 0 || column < 0)
            return QModelIndex();
        TemplateNode< Node > *parentNode = nodeFromIndex(parent);
        TemplateNode< Node > *childNode = parentNode->children.value(row);
        if (!childNode)
            return QModelIndex();
        return createIndex(row, column, childNode);
    }
    QModelIndex parent(const QModelIndex &child) const
    {
        if ( Node *node = nodeFromIndex(child) )
            if ( Node *parentNode = node->parent )
                if ( Node *grandparentNode = parentNode->parent )
                {
                    int row = grandparentNode->children.indexOf( parentNode );
                    return createIndex(row, 0, parentNode);
                }
        return QModelIndex();
    }

    int columnCount(const QModelIndex &) const {
        return 1;
    }
    int rowCount(const QModelIndex &parent) const
    {
        Node *parentNode = nodeFromIndex(parent);
        if (!parentNode)
            return 0;
        return parentNode->children.count();
    }

    QModelIndex indexFromNode(Node* node)
    {
        if ( node ) {
            int row = node->parent->children.indexOf( node );
            return createIndex(row, 0, node);
        }
        return QModelIndex();
    }

    void removeNode(Node* node)
    {
        if ( node->parent )
        {
            int row = node->parent->children.indexOf( node );
            QModelIndex index = indexFromNode( node );
            removeRows( row, 1, parent(index) );
        }
        else
            clear();
    }

    bool removeRows(int row, int count, const QModelIndex &parent)
    {
        int last = row + count - 1;
        beginRemoveRows(parent, row, last);

        Node* parentNode = nodeFromIndex(parent);
        if (parentNode && parentNode->children.size() > last)
        {
            for (int i = last; i >= row; i--)
            {
                Node* n = parentNode->children.at(i);
                if (n)
                    delete n;
                parentNode->children.removeAt(i);
            }
        }

        endRemoveRows();

        emit dataChanged(QModelIndex(), QModelIndex());
        return true;
    }

    QVariant data(const QModelIndex &index, int role) const
    {
        if ( index.isValid() && (role == Qt::DecorationRole ||
                                 role == Qt::DisplayRole || role == Qt::BackgroundRole ||
                                 role == Qt::StatusTipRole || role == Qt::ToolTipRole) )
            if ( SimpleNode< Node > *node = nodeFromIndex(index) )
            {
                if ( role == Qt::DecorationRole && index.column() == 0)
                {
                    switch (node->type) {
                    case SimpleNode< Node >::HTML:
                        return QIcon(":/images/html.png");
                    case SimpleNode< Node >::Image:
                        return QIcon(":/images/image.png");
                    case SimpleNode< Node >::Text:
                        return QIcon(":/images/text.png");
                    case SimpleNode< Node >::Folder:
                        return QIcon(":/images/folder.png");
                    case SimpleNode< Node >::Other:
                        return QIcon(":/images/other.png");
                    }
                }
                else if ( role == Qt::ToolTipRole || role == Qt::StatusTipRole )
                    return node->toolTip;
                else if ( role == Qt::DisplayRole )
                    return node->name;
                else if ( role == Qt::BackgroundRole )
                    switch ( node->state ) {
                    case SimpleNode< Node >::NoState: break;
                    case SimpleNode< Node >::New:
                        return QColor(150, 255, 150);
                    case SimpleNode< Node >::Edited:
                        return QColor(100, 135, 175);
                        //            case Node::UnBase:  return QVariant(); //return QColor(213,213,213); //QColor(Qt::gray);
                        //            case Node::InBase:  return QColor(150, 255, 150);
                    }
            }

        return QVariant();
    }
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole)
    {
        if ( role == Qt::EditRole && index.isValid() )
            if ( Node *node = nodeFromIndex(index) )
            {
                if ( role == Qt::EditRole )
                    node->name = value.toString();

                emit dataChanged(index, index);
                return true;
            }
        return false;
    }

    Node *nodeFromIndex(const QModelIndex &index) const
    {
        if (index.isValid()) {
            return static_cast<Node *>( index.internalPointer() );
        } else {
            return rootNode;
        }
    }
protected:
    Node *rootNode;
};

#endif // TREEMODEL_H
