#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QModelIndex>

namespace Ui {
class MainWindow;
}

class QProgressBar;
class QPushButton;

class FSWatcherThread;
class ParseThread;
class LogLabel;
class DirModel;
class DirNode;
class SubDirNode;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void show();
private slots:
    void hideProgress(bool visable = false);
    void on_actionChooseFolder_triggered();
    void setCurrentModelIndex(QModelIndex index, QModelIndex prev);
    void setCurrentSubModelIndex(QModelIndex index, QModelIndex prev);
    void parse(const QString& path, bool update = true);
    void parseFinished();
    void on_actionSave_triggered();
    void enableSave();
    void on_actionDel_triggered();

protected:
    void closeEvent(QCloseEvent *event);
private:
    void parseDirectory(QString path = QString());
    bool checkSave();
    void saveChanges(DirNode *node, SubDirNode *subNode);
    QString getFileText(const QString &fileName);
    Ui::MainWindow *ui;
    LogLabel* fileNameLbl;
    QProgressBar* pbar;
    QPushButton* cancelBtn;
    DirModel* model;
    FSWatcherThread* fsWatcherTh;
    ParseThread* parseThread;
};

#endif // MAINWINDOW_H
