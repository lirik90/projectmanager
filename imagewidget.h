#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

#include <QWidget>

class ImageWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ImageWidget(QWidget *parent = 0);
    void setPixmap(QPixmap pix);
protected:
    void paintEvent(QPaintEvent *event);
private:
    QPixmap img;
};

#endif // IMAGEWIDGET_H
