#include <QPainter>

#include "imagewidget.h"

ImageWidget::ImageWidget(QWidget *parent) :
    QWidget(parent)
{
}

void ImageWidget::setPixmap(QPixmap pix)
{
    img = pix;
    repaint();
}

void ImageWidget::paintEvent(QPaintEvent *event)
{
    QWidget::paintEvent(event);

    if (img.isNull())
        return;

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    QSize pixSize /*= img.size()*/, widSize = size();
//    pixSize.scale(event->rect().size(), Qt::KeepAspectRatio);

    QPixmap scaledPix = img.scaled(widSize,
                                   Qt::KeepAspectRatio,
                                   Qt::SmoothTransformation);

    pixSize = scaledPix.size();
    QPoint p;
    if (pixSize.width() < widSize.width())
        p.rx() = (widSize.width() - pixSize.width()) / 2;
    if (pixSize.height() < widSize.height())
        p.ry() = (widSize.height() - pixSize.height()) / 2;

    painter.drawPixmap(p, scaledPix);

}
