#include <QDebug>
#include <QFile>
#include <QMessageBox>

#include "dirmodel.h"
#include "fswatcherthread.h"

FSWatcherThread::FSWatcherThread(DirModel *dirModel, QObject *parent) :
    QThread(parent),
    fsWatcherPtr(nullptr),
    model(dirModel)
{

}

void FSWatcherThread::restart()
{
    quit();

    if ( isRunning() )
        wait();

    start();

    while ( !wait(1) && fsWatcherPtr == nullptr );
}

void FSWatcherThread::run()
{
    FSWatcher fsWatcher( model );
    connect(&fsWatcher, SIGNAL(needUpdate(QString)), SIGNAL(needUpdate(QString)));
    fsWatcherPtr = &fsWatcher;

    exec();
    fsWatcherPtr = nullptr;
}

FSWatcher::FSWatcher(DirModel *dirModel, QObject *parent) :
    QFileSystemWatcher(parent),
    model(dirModel)
{
    connect(this, SIGNAL(directoryChanged(QString)),
            SLOT(directoryChangedSlot(QString)));
    connect(this, SIGNAL(fileChanged(QString)),
            SLOT(fileChangedSlot(QString)));
}

void FSWatcher::fileChangedSlot(const QString &path)
{
    if ( QFile::exists( path ) )
    {
        addPath( path );
        QMetaObject::invokeMethod(model, "setFileState", Qt::QueuedConnection,
                                  Q_ARG(QString, path), Q_ARG(int, DirNode::Edited));
    }
    else
        QMetaObject::invokeMethod(model, "removeFile", Qt::QueuedConnection,
                                  Q_ARG(QString, path));
}

void FSWatcher::directoryChangedSlot(const QString &path)
{
    emit needUpdate( path );
}
