#ifndef DIRMODEL_H
#define DIRMODEL_H

#include <QDate>

#include "treemodel.h"

class SubDirNode : public SimpleNode< SubDirNode >
{
public:
    QString fileName;
};

class SubDirModel : public TreeModel< SubDirNode >
{
    Q_OBJECT
public:
    void setNodeState(SubDirNode* node, SubDirNode::State state);
    void setNodeState(const QString& path, SubDirNode::State state);
    SubDirNode *nodeFromPath(const QString& path);
public slots:
//    void insertNode(SubDirNode* node, SubDirNode* parent = nullptr);
};

class DirNode : public SimpleNode< DirNode >
{
public:
    DirNode() : SimpleNode< DirNode >(), active(false) {}
    QString descriptionFile;
    SubDirModel model;

    bool active;
    QDate from;
    QDate to;
};

class DirModel : public TreeModel< DirNode >
{
    Q_OBJECT
public:
    DirModel(QObject *parent = 0);
    void setNodeState(DirNode* node, DirNode::State state);
    void setNodeState(SubDirModel* subModel, DirNode::State state);
    void unstateNode(DirNode* node);

    DirNode *nodeFromSubModel(SubDirModel* subModel);
    DirNode *nodeFromPath(const QString& path, SubDirNode **subNodePtr = nullptr);

public slots:
    void setFileState(QString path, int state);
    void removeFile(QString path);
};


#endif // DIRMODEL_H
