#include <QDebug>
#include <QPainter>

#include "dirmodel.h"
#include "coloredcalendar.h"

ColoredCalendar::ColoredCalendar(QWidget *parent) :
    QCalendarWidget(parent), node(nullptr)
{

}

void ColoredCalendar::setColoredRange(DirNode *dirNode)
{
    node = dirNode;
}

void ColoredCalendar::paintCell(QPainter *painter, const QRect &rect, const QDate &date) const
{
    if ( node && node->from.isValid() && date >= node->from &&
         date <= (node->to.isValid() ? node->to : QDate::currentDate()) )
    {
        Qt::GlobalColor color = selectedDate() == date ? Qt::gray :
                                          node->active ? Qt::darkGreen : Qt::red;
        painter->fillRect(rect, QBrush( color ));
        painter->drawText(rect, Qt::AlignCenter, QString::number(date.day()));
    }
    else
        QCalendarWidget::paintCell(painter, rect, date);
}
