#ifndef PARSETHREAD_H
#define PARSETHREAD_H

#include <QThread>

class DirModel;
class DirNode;
class SubDirNode;
class QFileSystemWatcher;

class ParseThread : public QThread
{
    Q_OBJECT
public:
    explicit ParseThread(QString path, DirModel* treeModel, QFileSystemWatcher* watcher,
                         bool refresh = false, QObject *parent = 0);
private:
    void run();

    QString parsePath;
    DirModel* model;

    QFileSystemWatcher* fsWatcher;
    bool canceled;
    bool isRefresh;

signals:
    void message(QString);
public slots:
    void cancel() { canceled = true; }
private:
    void parseDir(QString path, DirNode *parentNode = nullptr, SubDirNode *parentItem = nullptr);
    QString cFileName;
};

#endif // PARSETHREAD_H
