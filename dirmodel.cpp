#include <QFileInfo>

#include "dirmodel.h"

DirModel::DirModel(QObject *parent) :
    TreeModel< DirNode >( parent )
{
}

void DirModel::setNodeState(DirNode *node, DirNode::State state)
{
    node->state = state;
    if ( node->parent )
        node->parent->state = state;
    emit dataChanged(QModelIndex(), QModelIndex());
}

void DirModel::setNodeState(SubDirModel *subModel, DirNode::State state)
{
    DirNode* node = nodeFromSubModel( subModel );
    if ( node )
        setNodeState(node, state);
}

void DirModel::unstateNode(DirNode *node)
{
    bool descEdited = true;
    for (DirNode* subNode : node->children) {
        if (subNode->state != DirNode::NoState) {
            descEdited = false;
            break;
        }
    }
    for (SubDirNode* subNode : node->model.getRootNode()->children) {
        if (subNode->state != SubDirNode::NoState) {
            descEdited = false;
            break;
        }
    }

    if ( descEdited )
        setNodeState( node, DirNode::NoState );
}

void DirModel::removeFile(QString path)
{
    goAllNode([&](DirNode* node) -> bool
    {
        SubDirNode* subNode = node->model.nodeFromPath( path );
        if ( subNode )
        {
            node->model.removeNode( subNode );
            return true;
        }
        return false;
    });
}

void DirModel::setFileState(QString path, int state)
{
    QFileInfo fi( path );
    bool isDesc = fi.fileName() == "%description.txt";

    goAllNode([&](DirNode* node) -> bool
    {
        if ( isDesc ) {
            if ( node->toolTip == fi.path() )
            {
                setNodeState( node, DirNode::State( state ) );
                return true;
            }
        } else {
            SubDirNode* subNode = node->model.nodeFromPath( path );
            if ( subNode )
            {
                node->model.setNodeState( subNode, SubDirNode::State( state ) );
                setNodeState( node, DirNode::State( state ) );
                return true;
            }
        }
        return false;
    });
}

DirNode *DirModel::nodeFromSubModel(SubDirModel *subModel)
{
    std::function< DirNode*(DirNode*) > findNode = [&](DirNode* parent) -> DirNode*
    {
        for (DirNode* node: parent->children)
        {
            if ( &(node->model) == subModel )
                return node;

            DirNode* finded = findNode( node );
            if ( finded )
                return finded;
        }

        return nullptr;
    };

    return findNode( rootNode );
}

DirNode *DirModel::nodeFromPath(const QString &path, SubDirNode** subNodePtr)
{
    DirNode *res = nullptr;
    QFileInfo fi( path );
    bool inSubDir = fi.fileName().at(0) == '%';

    goAllNode([&](DirNode* node) -> bool
    {
        if ( inSubDir )
        {
            SubDirNode* subNode = node->model.nodeFromPath( path );
            if ( subNode )
            {
                if ( subNodePtr )
                    *subNodePtr = subNode;
                res = node;
                return true;
            }
        }
        else if ( node->toolTip == path )
        {
            res = node;
            return true;
        }
        return false;
    });

    return res;
}

void SubDirModel::setNodeState(SubDirNode *node, SubDirNode::State state)
{
    node->state = state;
    if ( node->parent )
        node->parent->state = state;
    emit dataChanged(QModelIndex(), QModelIndex());
}

void SubDirModel::setNodeState(const QString &path, SubDirNode::State state)
{
    SubDirNode *subNode = nodeFromPath( path );
    if ( subNode )
        setNodeState(subNode, state);
}

SubDirNode *SubDirModel::nodeFromPath(const QString &path)
{
    std::function< SubDirNode*(SubDirNode*) > findNode
            = [&](SubDirNode* parent) -> SubDirNode*
    {
        for (SubDirNode* node: parent->children)
        {
            if ( node->toolTip == path )
                return node;

            SubDirNode* finded = findNode( node );
            if ( finded )
                return finded;
        }

        return nullptr;
    };

    return findNode( rootNode );
}
